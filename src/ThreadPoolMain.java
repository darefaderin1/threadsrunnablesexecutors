import com.example.ThreadPoolMain.ThreadPool;

public class ThreadPoolMain {

    public static void main(String[] args) throws Exception {

        ThreadPool threadPool = new ThreadPool(3, 10);
        System.out.println("before enter loop");
        for (int i=0; i<10; i++) {
            int taskNo = i;
            System.out.println("inside loop");
            threadPool.execute( () -> {
                System.out.println("inside execute");
                String message =
                        Thread.currentThread().getName()
                                + ": Task " + taskNo;
                System.out.println(message);
            });
            System.out.println("after execute");
        }
        System.out.println("after loop");
        threadPool.waitUntilAllTasksFinished();
        threadPool.stop();
    }
}
